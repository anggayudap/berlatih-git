<?php

require_once('animal.php');
require_once('Frog.php');
require_once('Ape.php');

$sheep = new Animal("shaun");

// echo $sheep->name; // "shaun"
// echo $sheep->legs; // 4
// echo $sheep->cold_blooded; // "no"


$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"
$sungokong->legs = 2;

$kodok = new Frog("buduk");
$kodok->jump(); // "hop hop"

/* OUTPUTNYA */

echo '<pre>Name : ' . $sheep->name .
    '<br>' .
    'Legs : ' . $sheep->legs .
    '<br>' .
    'Cold Blooded : ' . $sheep->cold_blooded .
    '</pre>';

echo '<pre>Name : ' . $kodok->name .
    '<br>' .
    'Legs : ' . $kodok->legs .
    '<br>' .
    'Cold Blooded : ' . $kodok->cold_blooded .
    '<br>' .
    'Jump : ' . $kodok->jump() .
    '</pre>';

echo '<pre>Name : ' . $sungokong->name .
    '<br>' .
    'Legs : ' . $sungokong->legs .
    '<br>' .
    'Cold Blooded : ' . $sungokong->cold_blooded .
    '<br>' .
    'Yell : ' . $sungokong->yell() .
    '</pre>';

