<?php
/* kalau ini CRUD pake query builder */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller {
    public function index() {
        // menampilkan list data para pemain film (boleh menggunakan table html atau bootstrap card)
        $cast = DB::table('cast')->get();
        return view('menu.casts.index', compact('cast'));
    }

    public function create() {
        // menampilkan form untuk membuat data pemain film baru
        return view('menu.casts.create');
    }

    public function store(Request $request) {
        // menyimpan data baru ke tabel Cast
//        dd ($request->all());
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio'  => 'required',
        ]);
        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio"  => $request["bio"],
        ]);
        return redirect('/cast');
    }

    public function show($id) {
        // menampilkan detail data pemain film dengan id tertentu
        $post = DB::table('cast')->where('id', $id)->first();
        return view('menu.casts.show', compact('post'));
    }

    public function edit($id) {
        // menampilkan form untuk edit pemain film dengan id tertentu
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('menu.casts.edit', compact('cast'));
    }

    public function update($id, Request $request) {
        // menyimpan perubahan data pemain film (update) untuk id tertentu
//        dd($request->all());
        $request->validate([
            'nama' => 'required',
            'umur'  => 'required',
            'bio'  => 'required',
        ]);

        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request["nama"],
                'umur'  => $request["umur"],
                'bio'  => $request["bio"],
            ]);

//        dd($query);
        return redirect('/cast');
    }

    public function destroy($id) {
        // menghapus data pemain film dengan id tertentu

        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');

    }
}
