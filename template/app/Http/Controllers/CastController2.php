<?php
/* kalau ini CRUD pake eloquent */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cast;
use Validator, Redirect, Response, File;

class CastController2 extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cast = Cast::all();
        return view('menu.casts.index', compact('cast'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('menu.casts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//                dd($request->all());

        $this->validate($request,[
            'nama' => 'required',
            'umur' => 'required',
            'bio'  => 'required',
            'foto'  => 'required',
        ]);
        $file_foto = $request->file('foto');
//        $file_foto2 = $request->foto;
//        $foto = $request->file('foto');
//        $foto2 = $request->foto;
//        dd($foto);
//        dd($foto2);
//        echo var_dump($foto);
//        echo var_dump($foto2);
//        die;
        $new_foto  = time().' - '.$file_foto->getClientOriginalName();
        Cast::create([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio"  => $request["bio"],
            "foto"  => $new_foto,
        ]);

        $file_foto->move('uploads/cast/', $new_foto);

        return redirect('/cast2');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cast = Cast::find($id);
        return view('menu.casts.show', compact('cast'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cast = Cast::find($id);
        return view('menu.casts.edit', compact('cast'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'umur'  => 'required',
            'bio'  => 'required',
        ]);

        $post = Cast::find($id);
        $post->nama = $request->nama;
        $post->umur = $request->umur;
        $post->bio = $request->bio;
        $post->update();
        return redirect('/cast2');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cast = Cast::find($id);
        $cast->delete();
        return redirect('/cast2');
    }
}
