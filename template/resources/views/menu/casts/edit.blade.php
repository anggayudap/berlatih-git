@extends('layout.master')

@section('title-menu')
    Kelola Data Cast
@endsection

@section('content')
    <div>
        <h2>Edit Cast {{$cast->id}}</h2>
        <form action="/cast2/{{$cast->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="title"
                       placeholder="Masukkan Nama">
                @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="number" class="form-control" name="umur" value="{{$cast->umur}}" id="umur"
                       placeholder="Masukkan Umur">
                @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <textarea class="form-control" name="bio" id="bio"
                          placeholder="Masukkan Bio">{{$cast->bio}}</textarea>
                @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
@endsection

@push('scripts')

@endpush
