@extends('layout.master')

@section('title-menu')
    Detail Data Cast {{$post->id}}
@endsection

@section('content')
    <h3 class="text-primary">{{$post->nama}}</h3>
    <small>{{$post->umur}} Tahun</small>
    <p>{{$post->bio}}</p>
@endsection

@push('scripts')

@endpush
