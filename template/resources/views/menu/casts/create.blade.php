@extends('layout.master')

@section('title-menu')
    Create Cast
@endsection

@section('content')
    <div>
        <h2>Tambah Data</h2>
        <form action="/cast2" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="title">Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama">
                @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Umur</label>
                <input type="number" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur">
                @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Bio</label>
                <textarea class="form-control" name="bio" id="bio" placeholder="Masukkan Bio"></textarea>
                @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Foto</label>
                <input type="file" class="form-control" id="foto" name="foto">
            </div>
            @error('foto')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
    </div>
@endsection

@push('scripts')
    <script>
    </script>
@endpush
