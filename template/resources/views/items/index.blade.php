@extends('layout.master')

@section('title-menu')
    Wilujeng Sumping
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Hari 13 - Memasangkan Template dengan Laravel Blade</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <p>Ini adalah halaman Dashboard pada web ini.</p>
            <p>Akses menu Table dan DataTable pada menu sidebar disamping kiri halaman ini.</p>
            <p>Angga Yuda Permana &copy; 2021 - JCC Laravel Batch 1</p>
        </div>
        <!-- /.card-body -->

    </div>
@endsection

@push('scripts')
    <script>
        console.log('hello JCC. ini adalah halaman depan');
    </script>
@endpush
