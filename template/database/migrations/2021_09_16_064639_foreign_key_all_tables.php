<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ForeignKeyAllTables extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('profile', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        Schema::table('kritik', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('no action')
                ->onDelete('no action');

            $table->foreign('film_id')
                ->references('id')->on('film')
                ->onUpdate('no action')
                ->onDelete('no action');
        });

        Schema::table('film', function (Blueprint $table) {
            $table->foreign('genre_id')
                ->references('id')->on('genre')
                ->onUpdate('no action')
                ->onDelete('no action');
        });

        Schema::table('peran', function (Blueprint $table) {
            $table->foreign('film_id')
                ->references('id')->on('film')
                ->onUpdate('no action')
                ->onDelete('no action');

            $table->foreign('cast_id')
                ->references('id')->on('cast')
                ->onUpdate('no action')
                ->onDelete('no action');
        });


        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('profile', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });
        Schema::table('kritik', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['film_id']);
        });
        Schema::table('film', function (Blueprint $table) {
            $table->dropForeign(['genre_id']);
        });
        Schema::table('peran', function (Blueprint $table) {
            $table->dropForeign(['film_id']);
            $table->dropForeign(['cast_id']);
        });
    }
}
