<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Hari 1 - Berlatih HTML</title>
</head>
<body>
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
    <h5 class="my-0 mr-md-auto font-weight-normal">Angga Yuda Permana</h5>
    <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="/">Home</a>
        <a class="p-2 text-dark" href="mailto:anggayudap@outlook.com">Email saya!</a>
    </nav>
    <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Cari JCC - Laravel" aria-label="Cari">
        <button class="btn btn-outline-success my-2 my-sm-0" type="button">Cari</button>
    </form>
</div>

<div class="pricing-header px-3 py-3 pt-md-2 pb-md-4 mx-auto">
    <form action="/welcome" method="post">
        @csrf
        <h1>Buat Account Baru!</h1>

        <h5>Sign Up Form</h5>
        <div class="form-group">
            <label>First Name:</label>
            <input type="text" class="form-control col-3" name="first_name">
        </div>
        <div class="form-group">
            <label>Last Name:</label>
            <input type="text" class="form-control col-3" name="last_name">
        </div>
        <div class="form-group">
            <label>Gender:</label>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="radio" value="male">
                <label class="form-check-label">Male</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="radio" value="female">
                <label class="form-check-label">Female</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="radio" value="other">
                <label class="form-check-label">Other</label>
            </div>

        </div>
        <div class="form-group">
            <label for="exampleFormControlSelect1">Nationality:</label>
            <select class="form-control col-3" id="exampleFormControlSelect1">
                <option>Indonesia</option>
                <option>Malaysia</option>
                <option>Brunei</option>
                <option>Australia</option>
                <option>Thailand</option>
            </select>
        </div>
        <div class="form-group">
            <label>Language Spoken:</label>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="bahasa_indonesia">
                <label class="form-check-label">Bahasa Indonesia</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="english">
                <label class="form-check-label">English</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="other">
                <label class="form-check-label">Other</label>
            </div>
        </div>
        <div class="form-group">
            <label for="exampleFormControlTextarea1">Bio:</label>
            <textarea class="form-control col-3" id="exampleFormControlTextarea1" rows="3"></textarea>
        </div>
        <button class="btn btn-primary" type="submit">Sign Up</button>
    </form>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</body>
</html>
