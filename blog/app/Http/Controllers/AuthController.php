<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller {
    public function index() {
        return view('register');
    }

    public function submit(Request $request) {
        $first_name = $request->first_name;
        $last_name = $request->last_name;
        $fullname = $first_name . ' ' . $last_name;
        return view('welcome', compact('fullname'));
    }
}
